/**
 * @file      funny.cpp
 * @brief     Header of
 * @date      Wed Apr  6 19:39:57 2022
 * @author    andres
 * @copyright BSD-3-Clause
 *
 * This module
 */

#include <cstddef>
#include <iostream>
#include <numeric>
#include <vector>

namespace funny {

template <typename T> size_t funny(T number) {
    static auto list = std::vector<T>();
    list.reserve(number);
    std::cout << "number is: " << number << "\n";
    if (number == 0) {
        return list.size();
    } else if (number < 0) {
        return 0;
    }
    number -= 1;
    list.push_back(number);

    // for (const auto& l : list) {
    //     std::cout << " " << l;
    // }
    std::cout << "\nsum: " << std::accumulate(std::begin(list), std::end(list), 0) << "\n";
    return funny(number);
}
} // namespace funny
