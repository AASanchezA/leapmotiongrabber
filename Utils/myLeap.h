#pragma once

#include <Leap.h>
#include <cstring>
#include <iostream>
#include <map>
#include <opencv2/xfeatures2d/nonfree.hpp>
#include <optional>

#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/features2d.hpp>
#include <utility>

namespace myLeap {

namespace ns {
struct Shape {
    int width;
    int height;
    int channels;
};

} // namespace ns

using namespace Leap;
using namespace cv;

class SampleListener : public Listener {
  public:
    virtual void onInit(const Controller&);
    virtual void onConnect(const Controller&);
    virtual void onDisconnect(const Controller&);
    virtual void onExit(const Controller&);
    virtual void onFrame(const Controller&);
    virtual void onFocusGained(const Controller&);
    virtual void onFocusLost(const Controller&);
    virtual void onDeviceChange(const Controller&);
    virtual void onServiceConnect(const Controller&);
    virtual void onServiceDisconnect(const Controller&);

  private:
};

const std::string fingerNames[] = {"Thumb", "Index", "Middle", "Ring", "Pinky"};
const std::string boneNames[] = {"Metacarpal", "Proximal", "Middle", "Distal"};
const std::string stateNames[] = {"STATE_INVALID", "STATE_START", "STATE_UPDATE", "STATE_END"};

void SampleListener::onInit(const Controller& controller) {
    std::cout << "Initialized" << std::endl;
}

void SampleListener::onConnect(const Controller& controller) {
    std::cout << "Connected" << std::endl;
    controller.enableGesture(Gesture::TYPE_CIRCLE);
    controller.enableGesture(Gesture::TYPE_KEY_TAP);
    controller.enableGesture(Gesture::TYPE_SCREEN_TAP);
    controller.enableGesture(Gesture::TYPE_SWIPE);
}

void SampleListener::onDisconnect(const Controller& controller) {
    // Note: not dispatched when running in a debugger.
    std::cout << "Disconnected" << std::endl;
}

void SampleListener::onExit(const Controller& controller) { std::cout << "Exited" << std::endl; }

std::optional<std::pair<Mat, Mat>> convert_to_cv(const ImageList& images) {
    if (images.isEmpty() || images.count() != 2) {
      return {};
    }
    // std::cout << "Got Images\n";
    auto right = images[0];
    auto left = images[1];
    ns::Shape const s_left = {left.width(), left.height(), 1};
    int bytesPerPixel = left.bytesPerPixel();
    ns::Shape const s_right = {right.width(), right.height(), 1};

    return std::make_pair(Mat(s_left.height, s_left.width, CV_8UC1, left.dataPointer()),
                          Mat(s_right.height, s_right.width, CV_8UC1, right.dataPointer()));
}

void display_concatenate(const Mat& left, const Mat& right) {
    std::cout << "Got Images\n";
    cv::namedWindow("Image", cv::WINDOW_NORMAL);
    Mat mat_dst(Size(left.cols * 2, left.rows), left.type(), Scalar::all(0));
    Mat mat_roi = mat_dst(Rect(0, 0, left.cols, left.rows));
    left.copyTo(mat_roi);
    mat_roi = mat_dst(Rect(left.cols, 0, right.cols, right.rows));
    right.copyTo(mat_roi);
    cv::imshow("Image", mat_dst);
    cv::waitKey(1);
}

cv::Mat sift_calculator(const Mat& input) {

    // cv::Ptr<cv::SIFT> siftPtr = cv::SIFT::create();
    cv::Ptr<cv::xfeatures2d::SURF> siftPtr = cv::xfeatures2d::SURF::create();
    std::vector<cv::KeyPoint> keypoints;
    siftPtr->detect(input, keypoints);

    // Add results to image and save.
    cv::Mat output;
    cv::drawKeypoints(input, keypoints, output);
    return output;
}

void SampleListener::onFrame(const Controller& controller) {
    // Get the most recent frame and report some basic information
    const Frame frame = controller.frame();
    std::cout << "Frame id: " << frame.id() << ", timestamp: " << frame.timestamp()
              << ", hands: " << frame.hands().count()
              << ", extended fingers: " << frame.fingers().extended().count()
              << ", tools: " << frame.tools().count() << ", gestures: " << frame.gestures().count()
              << std::endl;

    HandList hands = frame.hands();
    for (HandList::const_iterator hl = hands.begin(); hl != hands.end(); ++hl) {
        // Get the first hand
        const Hand hand = *hl;
        std::string handType = hand.isLeft() ? "Left hand" : "Right hand";
        std::cout << std::string(2, ' ') << handType << ", id: " << hand.id()
                  << ", palm position: " << hand.palmPosition() << std::endl;
        // Get the hand's normal vector and direction
        const Vector normal = hand.palmNormal();
        const Vector direction = hand.direction();

        // Calculate the hand's pitch, roll, and yaw angles
        std::cout << std::string(2, ' ') << "pitch: " << direction.pitch() * RAD_TO_DEG
                  << " degrees, "
                  << "roll: " << normal.roll() * RAD_TO_DEG << " degrees, "
                  << "yaw: " << direction.yaw() * RAD_TO_DEG << " degrees" << std::endl;

        // Get the Arm bone
        Arm arm = hand.arm();
        std::cout << std::string(2, ' ') << "Arm direction: " << arm.direction()
                  << " wrist position: " << arm.wristPosition()
                  << " elbow position: " << arm.elbowPosition() << std::endl;

        // Get fingers
        const FingerList fingers = hand.fingers();
        for (FingerList::const_iterator fl = fingers.begin(); fl != fingers.end(); ++fl) {
            const Finger finger = *fl;
            std::cout << std::string(4, ' ') << fingerNames[finger.type()]
                      << " finger, id: " << finger.id() << ", length: " << finger.length()
                      << "mm, width: " << finger.width() << std::endl;

            // Get finger bones
            for (int b = 0; b < 4; ++b) {
                Bone::Type boneType = static_cast<Bone::Type>(b);
                Bone bone = finger.bone(boneType);
                std::cout << std::string(6, ' ') << boneNames[boneType]
                          << " bone, start: " << bone.prevJoint() << ", end: " << bone.nextJoint()
                          << ", direction: " << bone.direction() << std::endl;
            }
        }
    }

    // Get tools
    const ToolList tools = frame.tools();
    for (ToolList::const_iterator tl = tools.begin(); tl != tools.end(); ++tl) {
        const Tool tool = *tl;
        std::cout << std::string(2, ' ') << "Tool, id: " << tool.id()
                  << ", position: " << tool.tipPosition() << ", direction: " << tool.direction()
                  << std::endl;
    }

    // Get gestures
    const GestureList gestures = frame.gestures();
    for (int g = 0; g < gestures.count(); ++g) {
        Gesture gesture = gestures[g];

        switch (gesture.type()) {
        case Gesture::TYPE_CIRCLE: {
            CircleGesture circle = gesture;
            std::string clockwiseness;

            if (circle.pointable().direction().angleTo(circle.normal()) <= PI / 2) {
                clockwiseness = "clockwise";
            } else {
                clockwiseness = "counterclockwise";
            }

            // Calculate angle swept since last frame
            float sweptAngle = 0;
            if (circle.state() != Gesture::STATE_START) {
                CircleGesture previousUpdate =
                    CircleGesture(controller.frame(1).gesture(circle.id()));
                sweptAngle = (circle.progress() - previousUpdate.progress()) * 2 * PI;
            }
            std::cout << std::string(2, ' ') << "Circle id: " << gesture.id()
                      << ", state: " << stateNames[gesture.state()]
                      << ", progress: " << circle.progress() << ", radius: " << circle.radius()
                      << ", angle " << sweptAngle * RAD_TO_DEG << ", " << clockwiseness
                      << std::endl;
            break;
        }
        case Gesture::TYPE_SWIPE: {
            SwipeGesture swipe = gesture;
            std::cout << std::string(2, ' ') << "Swipe id: " << gesture.id()
                      << ", state: " << stateNames[gesture.state()]
                      << ", direction: " << swipe.direction() << ", speed: " << swipe.speed()
                      << std::endl;
            break;
        }
        case Gesture::TYPE_KEY_TAP: {
            KeyTapGesture tap = gesture;
            std::cout << std::string(2, ' ') << "Key Tap id: " << gesture.id()
                      << ", state: " << stateNames[gesture.state()]
                      << ", position: " << tap.position() << ", direction: " << tap.direction()
                      << std::endl;
            break;
        }
        case Gesture::TYPE_SCREEN_TAP: {
            ScreenTapGesture screentap = gesture;
            std::cout << std::string(2, ' ') << "Screen Tap id: " << gesture.id()
                      << ", state: " << stateNames[gesture.state()]
                      << ", position: " << screentap.position()
                      << ", direction: " << screentap.direction() << std::endl;
            break;
        }
        default:
            std::cout << std::string(2, ' ') << "Unknown gesture type." << std::endl;
            break;
        }
    }

    if (!frame.hands().isEmpty() || !gestures.isEmpty()) {
        std::cout << std::endl;
    }

    if (!frame.images().isEmpty()) {
        auto res = convert_to_cv(frame.images());
        if(res) {
          auto left = sift_calculator(res->first);
          auto right = sift_calculator(res->second);
          display_concatenate(left, right);
        }
    }
}

void SampleListener::onFocusGained(const Controller& controller) {
    std::cout << "Focus Gained" << std::endl;
}

void SampleListener::onFocusLost(const Controller& controller) {
    std::cout << "Focus Lost" << std::endl;
}

void SampleListener::onDeviceChange(const Controller& controller) {
    std::cout << "Device Changed" << std::endl;
    const DeviceList devices = controller.devices();

    for (int i = 0; i < devices.count(); ++i) {
        std::cout << "id: " << devices[i].toString() << std::endl;
        std::cout << "  isStreaming: " << (devices[i].isStreaming() ? "true" : "false")
                  << std::endl;
    }
}

void SampleListener::onServiceConnect(const Controller& controller) {
    std::cout << "Service Connected" << std::endl;
}

void SampleListener::onServiceDisconnect(const Controller& controller) {
    std::cout << "Service Disconnected" << std::endl;
}

} // namespace myLeap
