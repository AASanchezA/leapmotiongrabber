// Some stuff
#include "Utils.h"

#include "Table.h"

#include <cmath>
#include <cstdlib>
#include <iostream>

double mySqrt(double number) {
    std::cout << "My Custom implementation of square root\n";
    return std::sqrt(number);
}