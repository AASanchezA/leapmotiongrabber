#[=======================================================================[.rst:
FindLeapSDK
----------

Try to fin the Leap SDK from leap-motion-sensor https://developer.leapmotion.com/

::

  This module defines the following variables

  LEAPSDK_FOUND - Was Leap SDK found
  LEAPSDK_INCLUDE_DIRS - the Leap SDK include directories
  LEAPSDK_LIBRARIES - Link to this Leap SDK

::

  LEAPSDK_ROOT - Can be set to LeapSDK install path 

#]=======================================================================]
message("<FindLeapSDK.cmake>")

set(LEAPSDK_SEARCH_PATH
  /usr/share/doc/leap-motion-sdk/samples
  /usr/include
  /usr/lib/Leap
  ${LEAPSDK_ROOT}
  )

find_path(LEAPSDK_INCLUDE_DIRS Leap.h
  HINTS
  $ENV{LEAPSDK_DIR}
  PATH_SUFFIXES include/leap include
  PATHS ${LEAPSDK_SEARCH_PATH}
  )

find_library(LEAPSDK_LIBRARIES
  NAMES libLeap.so
  HINTS
  $ENV{LEAPSDK_DIR}
  PATH_SUFFIXES lib64 lib
  PATHS ${LEAPSDK_SEARCH_PATH}
  )


  if(LEAPSDK_INCLUDE_DIRS)
    message("Found Include ${LEAPSDK_INCLUDE_DIR}")
  endif()

  if(LEAPSDK_LIBRARIES)
    message("Found Library ${LEAPSDK_LIBRARY}")
  endif()

  if(LEAPSDK_INCLUDE_DIRS AND LEAPSDK_LIBRARIES)
    message("Leap sdk found!")
    set(LEAPSDK_FOUND ON BOOL)
   endif()



