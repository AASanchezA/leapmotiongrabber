cmake_minimum_required(VERSION 3.21)
project (App)

# Add Version Number
set (App_VERSION_MAJOR 1)
set (App_VERSION_MINOR 0)

# system introspection
include(CheckFunctionExists)
check_function_exists(log HAVE_LOG)
check_function_exists(exp HAVE_EXP)

# Add header file to pass  Cmake variables
configure_file(
    "${PROJECT_SOURCE_DIR}/AppConfig.h.in"
    "${PROJECT_BINARY_DIR}/AppConfig.h"
)
include_directories("${PROJECT_BINARY_DIR}")

# Options
option(USE_UTILS
        "Use Utils from Users" ON)

if(USE_UTILS)
    # Add custom library
    include_directories("${PROJECT_SOURCE_DIR}/Utils")
    add_subdirectory(Utils)
    set( EXTRA_LIBS ${EXTRA_LIBS} Utils)
endif(USE_UTILS)


add_executable(App main.cpp)
target_link_libraries(App ${EXTRA_LIBS})

install(TARGETS App DESTINATION bin)
install(FILES "${PROJECT_BINARY_DIR}/AppConfig.h" DESTINATION include)

include(CTest)

# define a macro
macro( do_test arg result)
    add_test(AppCompare${arg} App ${arg})
    set_tests_properties(AppCompare${arg} PROPERTIES PASS_REGULAR_EXPRESSION ${result})
endmacro(do_test)

# does the application run
add_test(AppRuns App 25)

do_test(25 "25 is 5")

do_test(-25 "-25 is 0")

do_test(0.0001 "0.0001 is 0.01")

add_test(AppUsage App)
set_tests_properties(AppUsage PROPERTIES PASS_REGULAR_EXPRESSION "Usage:.*number")

# Build a CPack driven Installer package
include(InstallRequiredSystemLibraries)
set(CPACK_RESOURCE_FILE_LICENSE
    "${CMAKE_CURRENT_SOURCE_DIR}/License.txt")
set(CPACK_PACKAGE_VERSION_MAJOR "${App_VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${App_VERSION_MINOR}")
include(CPack)

# add Project name to dashboard
set(CTEST_PROJECT_NAME "App")
