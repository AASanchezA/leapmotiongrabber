// A simple program that computes the square root of a number
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <limits>
#include <numeric>
#include <string>
#include <sys/types.h>
#include <vector>

#include "AppConfig.h"

#ifdef USE_UTILS
#include "Utils.h"
#endif

#include <cstring>
#include <iostream>

#include <myLeap.h>
#include "funny.h"

using namespace std;

template <typename T> void print_limts() {
    cout << "min: " << numeric_limits<T>::lowest() << " max: " << numeric_limits<T>::max() << "\n";
    cout << "test: " << numeric_limits<double_t>::max() << "\n";
}

using namespace myLeap;

int main(int argc, char* argv[]) {
    if (argc < 2) {
        cout << "Usage: " << argv[0] << " number\n";
        cout << "Major Version : " << App_VERSION_MAJOR << "\n";
        cout << "Minor Version : " << App_VERSION_MINOR << "\n";
        return 1;
    }

    // Create a sample listener and controller
    SampleListener listener;
    Controller controller;

    // Have the sample listener receive events from the controller
    controller.addListener(listener);

    controller.setPolicy(Leap::Controller::POLICY_IMAGES);
    if (argc > 1 && strcmp(argv[1], "--bg") == 0) {
    }

    // Keep this process running until Enter is pressed
    std::cout << "Press Enter to quit..." << std::endl;
    std::cin.get();

    // Remove the sample listener when done
    controller.removeListener(listener);

    return 0;
}
